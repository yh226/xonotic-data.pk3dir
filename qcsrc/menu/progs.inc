#include "../common/util-pre.qh"
#include "../dpdefs/menudefs.qh"
#include "../dpdefs/keycodes.qh"
#include "../common/util-post.qh"

#include "../lib/_all.inc"

#include "oo/classes.qc"

#include "draw.qc"
#include "menu.qc"

#include "command/all.qc"

#include "xonotic/util.qc"

#include "../common/campaign_file.qc"
#include "../common/campaign_setup.qc"
#include "../common/mapinfo.qc"
#include "../common/playerstats.qc"
#include "../common/util.qc"

#include "../common/items/all.qc"
#include "../common/monsters/all.qc"
#include "../common/mutators/all.qc"
#include "../common/vehicles/all.qc"
#include "../common/weapons/all.qc"

#if BUILD_MOD
#include "../../mod/menu/progs.inc"
#endif
