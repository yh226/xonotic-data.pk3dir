#ifndef NADES_H
#define NADES_H

#include "teams.qh"

.float healer_lifetime;
.float healer_radius;

// use slots 70-100
const int PROJECTILE_NADE = 71;
const int PROJECTILE_NADE_BURN = 72;
const int PROJECTILE_NADE_NAPALM = 73;
const int PROJECTILE_NADE_NAPALM_BURN = 74;
const int PROJECTILE_NAPALM_FOUNTAIN = 75;
const int PROJECTILE_NADE_ICE = 76;
const int PROJECTILE_NADE_ICE_BURN = 77;
const int PROJECTILE_NADE_TRANSLOCATE = 78;
const int PROJECTILE_NADE_SPAWN = 79;
const int PROJECTILE_NADE_HEAL = 80;
const int PROJECTILE_NADE_HEAL_BURN = 81;
const int PROJECTILE_NADE_MONSTER = 82;
const int PROJECTILE_NADE_MONSTER_BURN = 83;

void RegisterNades();
const int NADES_MAX = 8;
entity NADES[NADES_MAX], NADES_first, NADES_last;
int NADES_COUNT;
#define REGISTER_NADE(id) REGISTER(RegisterNades, NADE_TYPE, NADES, NADES_COUNT, id, m_id, NEW(Nade))
REGISTER_REGISTRY(RegisterNades)

CLASS(Nade, Object)
    ATTRIB(Nade, m_id, int, 0)
    ATTRIB(Nade, m_color, vector, '0 0 0')
    ATTRIB(Nade, m_name, string, _("Grenade"))
    ATTRIB(Nade, m_icon, string, "nade_normal")
    ATTRIBARRAY(Nade, m_projectile, int, 2)
    ATTRIBARRAY(Nade, m_trail, string, 2)
    METHOD(Nade, display, void(entity this, void(string name, string icon) returns)) {
        returns(this.m_name, sprintf("/gfx/hud/%s/%s", cvar_string("menu_skin"), this.m_icon));
    }
ENDCLASS(Nade)

REGISTER_NADE(Null);

#define NADE_PROJECTILE(i, projectile, trail) do { \
    this.m_projectile[i] = projectile; \
    this.m_trail[i] = trail; \
} while (0)

REGISTER_NADE(NORMAL) {
    this.m_color = '1 1 1';
    NADE_PROJECTILE(0, PROJECTILE_NADE, string_null);
    NADE_PROJECTILE(1, PROJECTILE_NADE_BURN, string_null);
}

REGISTER_NADE(NAPALM) {
    this.m_color = '2 0.5 0';
    this.m_name = _("Napalm grenade");
    this.m_icon = "nade_napalm";
    NADE_PROJECTILE(0, PROJECTILE_NADE_NAPALM, "TR_ROCKET");
    NADE_PROJECTILE(1, PROJECTILE_NADE_NAPALM_BURN, "spiderbot_rocket_thrust");
}

REGISTER_NADE(ICE) {
    this.m_color = '0 0.5 2';
    this.m_name = _("Ice grenade");
    this.m_icon = "nade_ice";
    NADE_PROJECTILE(0, PROJECTILE_NADE_ICE, "TR_NEXUIZPLASMA");
    NADE_PROJECTILE(1, PROJECTILE_NADE_ICE_BURN, "wakizashi_rocket_thrust");
}

REGISTER_NADE(TRANSLOCATE) {
    this.m_color = '1 0 1';
    this.m_name = _("Translocate grenade");
    this.m_icon = "nade_translocate";
    NADE_PROJECTILE(0, PROJECTILE_NADE_TRANSLOCATE, "TR_CRYLINKPLASMA");
    NADE_PROJECTILE(1, PROJECTILE_NADE_TRANSLOCATE, "TR_CRYLINKPLASMA");
}

REGISTER_NADE(SPAWN) {
    this.m_color = '1 0.9 0';
    this.m_name = _("Spawn grenade");
    this.m_icon = "nade_spawn";
    NADE_PROJECTILE(0, PROJECTILE_NADE_SPAWN, "nade_yellow");
    NADE_PROJECTILE(1, PROJECTILE_NADE_SPAWN, "nade_yellow");
}

REGISTER_NADE(HEAL) {
    this.m_color = '1 0 0';
    this.m_name = _("Heal grenade");
    this.m_icon = "nade_heal";
    NADE_PROJECTILE(0, PROJECTILE_NADE_HEAL, "nade_red");
    NADE_PROJECTILE(1, PROJECTILE_NADE_HEAL_BURN, "nade_red_burn");
}

REGISTER_NADE(MONSTER) {
    this.m_color = '0.25 0.75 0';
    this.m_name = _("Monster grenade");
    this.m_icon = "nade_monster";
    NADE_PROJECTILE(0, PROJECTILE_NADE_MONSTER, "nade_red");
    NADE_PROJECTILE(1, PROJECTILE_NADE_MONSTER_BURN, "nade_red_burn");
}

entity Nade_FromProjectile(float proj)
{
    FOREACH(NADES, true, LAMBDA(
        for (int j = 0; j < 2; j++)
        {
            if (it.m_projectile[j] == proj) return it;
        }
    ));
    return NADE_TYPE_Null;
}

string Nade_TrailEffect(int proj, float nade_team)
{
    switch (proj)
    {
        case PROJECTILE_NADE:       return EFFECT_NADE_TRAIL(nade_team).eent_eff_name;
        case PROJECTILE_NADE_BURN:  return EFFECT_NADE_TRAIL_BURN(nade_team).eent_eff_name;
    }
    FOREACH(NADES, true, LAMBDA(
        for (int j = 0; j < 2; j++)
        {
            if (it.m_projectile[j] == proj)
            {
                string trail = it.m_trail[j];
                if (trail) return trail;
                break;
            }
        }
    ));
    return string_null;
}

#ifdef SVQC
float healer_send(entity to, int sf);
#endif

#endif
